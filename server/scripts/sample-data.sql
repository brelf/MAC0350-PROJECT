--------------------------------------------------------------------------------
--- Insere disciplinas
--------------------------------------------------------------------------------

INSERT INTO
public.disciplinas(
  codigo_disciplina,
  creditos_aula,
  creditos_trabalho,
  carga_horaria,
  ativacao, 
  nome, 
  ementa)
VALUES
('MAC0101', 4, 0, 60, '2015-01-01', 
'Integração na Universidade e na Profissão', 'Apresentar o Bacharelado em Ciência da Computação (BCC) aos estudantes que acabaram de ingressar no curso. Orientar os estudantes sobre as várias possibilidades de formação como indivíduos, como cidadãos e como cientistas da computação que a Universidade de São Paulo, o Instituto de Matemática e Estatística e o Departamento de Ciência da Computação (DCC) oferecem. Expandir os conceitos que os estudantes tem sobre Ciência da Computação e apresentá-los aos recursos disponibilizados a eles pelo DCC. Além disso, esta disciplina é um lugar onde os estudantes podem constituir grupos de interesse e estender seus horizontes dentro do DCC.' ),
('MAC0110', 4, 0, 60, '1998-01-01',
'Introdução à Computação', 'Introduzir a programação de computadores através do estudo de uma linguagem algorítmica e de exercícios práticos.' ),
('MAC0121', 4, 0, 60, '2015-01-01', 
'Algoritmos e Estruturas de Dados I', 'Introduzir técnicas básicas de programação, estruturas de dados básicas, e noções de projeto e análise de algoritmos, por meio de exemplos.' ),
('MAC0350', 4, 0, 60, '2017-06-01', 
'Introdução ao Desenvolvimento de Sistemas de Software', '  Tornar os alunos capazes de projetar, implementar e testar sistemas de software avançados, usando conceitos e técnicas de engenharia de software e banco de dados de maneira integrada e evolutiva. Os sistemas serão desenvolvidos em times, de maneira colaborativa, e de modo a priorizar clareza de código e extensibilidade.' ),
('MAC0426', 4, 0, 60, '1998-01-01',
'Sistemas de Bancos de Dados', 'Expor os principais componentes da arquitetura dos sistemas gerenciadores de bancos de dados relacionais. Introduzir outras abordagens de representação e gerenciamento de dados, como os dados semi-estruturados e os bancos de dados não relacionais.' ),
('OBR0001', 5, 0, 100, '1998-01-01', 'Matéria Obrigatoria 1', 'Essa matéria deve ser feita'),
('LIV0001', 5, 0, 100, '1998-01-01', 'Matéria da FFLECH', 'Você achou uma esfera do dragao!');

INSERT INTO 
public.grade_obrigatoria (
  id_disciplina, 
  ano_grade_obrigatoria
)
VALUES
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'OBR0001' LIMIT 1), '01-01-2015'); 

INSERT INTO
public.grade_optativa (
  id_disciplina, 
  ano_grade_optativa,
  eletiva 
 )
VALUES 
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0101' LIMIT 1), '01-01-2015', True), 
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0110' LIMIT 1) ,'01-01-2015', True), 
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0121' LIMIT 1) ,'01-01-2015', True), 
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0350' LIMIT 1) ,'01-01-2015', True), 
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0426' LIMIT 1) ,'01-01-2015', True),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'LIV0001' LIMIT 1) ,'01-01-2015', False);


--------------------------------------------------------------------------------
--- Insere Trilhas 
--------------------------------------------------------------------------------
INSERT INTO 
public.trilhas(
  id_trilha,
  codigo_trilha, 
  nome)
VALUES
( '1', 'T01', 'Trilha 1'), 
( '2', 'T02', 'Trilha 2'), 
( '3', 'T03', 'Trilha 3'),
( '5', 'LIV', 'Livres');

--------------------------------------------------------------------------------
--- Insere Modulos 
--------------------------------------------------------------------------------
INSERT INTO
public.modulos (
  id_modulo,
  codigo_modulo, 
  id_trilha)
VALUES
( '1', 'M01', 1), 
( '2', 'M02', 1),
( '3', 'M03', 2), 
( '4', 'M04', 3),
( '6', 'LIV', 5);



INSERT INTO
public.optativas_compoem_modulos (
  id_disciplina, 
  ano_grade_optativa,
  id_modulo
)
VALUES 
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0101' LIMIT 1), '01-01-2015', '1' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0110' LIMIT 1), '01-01-2015', '1' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0121' LIMIT 1), '01-01-2015', '1' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0121' LIMIT 1), '01-01-2015', '2' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0350' LIMIT 1), '01-01-2015', '2' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0350' LIMIT 1), '01-01-2015', '3' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0426' LIMIT 1), '01-01-2015', '4' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'LIV0001' LIMIT 1), '01-01-2015', '6' );
--------------------------------------------------------------------------------
--- Insere pessoas em seus respectivos cargos
--------------------------------------------------------------------------------

INSERT INTO
public.pessoas(nome)
VALUES
( 'Jef'    ),
( 'Décio'  ),
( 'Renato' );

INSERT INTO
public.administradores(id_pessoa, nusp_administrador, data_inicio)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  '0000001',
  '2018-01-01'
);

INSERT INTO
public.professores(id_pessoa, nusp_professor, departamento)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  '0000001',
  'Departamento de Ciência da Computação'
);

INSERT INTO
public.alunos(id_pessoa, nusp_aluno, turma_ingresso)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
  '0000002',
  '2015-03-01'
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Renato' LIMIT 1),
  '0000003',
  '2012-03-01'
);

--------------------------------------------------------------------------------
--- Aluno faz Disciplina
--------------------------------------------------------------------------------

INSERT INTO 
public.professores_oferecem_disciplinas (
  id_pessoa,
  nusp_professor,
  id_disciplina,  
  ano_semestre)
VALUES
((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1), 
 (SELECT nusp_professor FROM public.professores
       JOIN public.pessoas USING (id_pessoa)
       WHERE nome = 'Jef' LIMIT 1),
 (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0101' LIMIT 1), 
 1
),
((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1), 
 (SELECT nusp_professor FROM public.professores
       JOIN public.pessoas USING (id_pessoa)
       WHERE nome = 'Jef' LIMIT 1),
 (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0110' LIMIT 1), 
 1
),
((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1), 
 (SELECT nusp_professor FROM public.professores
       JOIN public.pessoas USING (id_pessoa)
       WHERE nome = 'Jef' LIMIT 1),
 (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0121' LIMIT 1), 
 1
),
((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1), 
 (SELECT nusp_professor FROM public.professores
       JOIN public.pessoas USING (id_pessoa)
       WHERE nome = 'Jef' LIMIT 1),
 (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0350' LIMIT 1), 
 1
),
((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1), 
 (SELECT nusp_professor FROM public.professores
       JOIN public.pessoas USING (id_pessoa)
       WHERE nome = 'Jef' LIMIT 1),
 (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0426' LIMIT 1), 
 1
),
((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1), 
 (SELECT nusp_professor FROM public.professores
       JOIN public.pessoas USING (id_pessoa)
       WHERE nome = 'Jef' LIMIT 1),
 (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'OBR0001' LIMIT 1), 
 1
),
((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1), 
 (SELECT nusp_professor FROM public.professores
       JOIN public.pessoas USING (id_pessoa)
       WHERE nome = 'Jef' LIMIT 1),
 (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'LIV0001' LIMIT 1), 
 1
);


INSERT INTO
alunos_cursam_disciplinas (
  id_pessoa,
  nusp_aluno,
  id_oferecimento,
  nota
)
VALUES 
((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
 (SELECT nusp_aluno FROM public.alunos
       JOIN public.pessoas USING (id_pessoa)
       WHERE nome = 'Décio' LIMIT 1),
 (SELECT id_oferecimento FROM public.professores_oferecem_disciplinas WHERE id_disciplina IN 
    (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0101' LIMIT 1) LIMIT 1),
  10
),
((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
 (SELECT nusp_aluno FROM public.alunos
       JOIN public.pessoas USING (id_pessoa)
       WHERE nome = 'Décio' LIMIT 1),
 (SELECT id_oferecimento FROM public.professores_oferecem_disciplinas WHERE id_disciplina IN 
    (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0110' LIMIT 1) LIMIT 1),
  10
),
((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
 (SELECT nusp_aluno FROM public.alunos
       JOIN public.pessoas USING (id_pessoa)
       WHERE nome = 'Décio' LIMIT 1),
 (SELECT id_oferecimento FROM public.professores_oferecem_disciplinas WHERE id_disciplina IN 
    (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'OBR0001' LIMIT 1) LIMIT 1),
  10
);

INSERT INTO
alunos_cursam_disciplinas (
  id_pessoa,
  nusp_aluno,
  id_oferecimento
)
VALUES 
((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
 (SELECT nusp_aluno FROM public.alunos
       JOIN public.pessoas USING (id_pessoa)
       WHERE nome = 'Décio' LIMIT 1),
 (SELECT id_oferecimento FROM public.professores_oferecem_disciplinas WHERE id_disciplina IN 
    (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0121' LIMIT 1) LIMIT 1)
),
((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
 (SELECT nusp_aluno FROM public.alunos
       JOIN public.pessoas USING (id_pessoa)
       WHERE nome = 'Décio' LIMIT 1),
 (SELECT id_oferecimento FROM public.professores_oferecem_disciplinas WHERE id_disciplina IN
    (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0350' LIMIT 1) LIMIT 1)
),
((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
 (SELECT nusp_aluno FROM public.alunos
       JOIN public.pessoas USING (id_pessoa)
       WHERE nome = 'Décio' LIMIT 1),
 (SELECT id_oferecimento FROM public.professores_oferecem_disciplinas WHERE id_disciplina IN
    (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'LIV0001' LIMIT 1) LIMIT 1)
);

--------------------------------------------------------------------------------
--- Disciplina entra na grade
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
--- Cria estrutura de autenticação
--------------------------------------------------------------------------------

INSERT INTO
auth.usuarios(email_usuario, senha, expira)
VALUES
( 'jef@ime.usp.br', '12345', '2019-01-01' ),
( 'decio@ime.usp.br', '12345', '2019-01-01' ),
( 'renatocf@ime.usp.br', '12345', '2019-01-01' );

INSERT INTO
public.pessoas_geram_usuarios(id_pessoa, id_usuario)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br')
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'decio@ime.usp.br')
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Renato' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'renatocf@ime.usp.br')
);

INSERT INTO
auth.perfis(nome_perfil)
VALUES
( 'administrador' ), -- 1
( 'professor' ), -- 2
( 'aluno' ); -- 3

INSERT INTO
auth.servicos(caminho_servico)
VALUES
( '/'             ), -- 1
( '/index'        ), -- 2
( '/estrutura'    ), -- 3
( '/plano'        ), -- 4
( '/professores'  ), -- 5
( '/pessoas' ), -- 6
( '/professores_oferecem_disciplinas'    ), -- 7
( '/pessoas_geram_usuarios'   ), -- 8
( '/professores/show'   ); -- 9

INSERT INTO
auth.usuarios_possuem_perfis(id_usuario, id_perfil, expira)
VALUES
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'decio@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'renatocf@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  '2019-01-01'
);

INSERT INTO
auth.perfis_acessam_servicos(id_perfil, id_servico, expira)
VALUES
-- Administrador
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/professores'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/pessoas'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/professores_oferecem_disciplinas'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/pessoas_geram_usuarios'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/professores/show'),
  '2019-01-01'
),
-- Professor
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
),
-- Estudante
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
);

INSERT INTO alunos_planejam_disciplinas(id_pessoa, nusp_aluno, id_disciplina, ano_semestre) VALUES ((SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1), '0000002', (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0426' LIMIT 1), 1);
