CREATE OR REPLACE FUNCTION
public.get_disciplinas_feitas(aluno bigint)
  RETURNS TABLE(id_disciplina bigint)
  LANGUAGE SQL
AS $$
  SELECT oferecimento.id_disciplina
  FROM 
    public.alunos_cursam_disciplinas as cursar,
    public.professores_oferecem_disciplinas as oferecimento,
    public.disciplinas as disciplinas
    WHERE 
        cursar.id_oferecimento = oferecimento.id_oferecimento AND
        disciplinas.id_disciplina = oferecimento.id_disciplina AND
        cursar.id_pessoa = aluno AND
        (cursar.nota IS NOT NULL)

$$;

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION
public.get_disciplinas_fazendo(aluno bigint)
  RETURNS TABLE(id_disciplina bigint, creditos integer)
  LANGUAGE SQL
AS $$
  SELECT oferecimento.id_disciplina, disciplinas.creditos_aula + disciplinas.creditos_trabalho
  FROM 
    public.alunos_cursam_disciplinas as cursar,
    public.professores_oferecem_disciplinas as oferecimento,
    public.disciplinas as disciplinas
    WHERE 
        cursar.id_oferecimento = oferecimento.id_oferecimento AND
        disciplinas.id_disciplina = oferecimento.id_disciplina AND
        cursar.id_pessoa = aluno AND
        cursar.nota IS NULL

$$;

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION
public.get_id_pessoa(email varchar(256))
  RETURNS TABLE(id_pessoa bigint)
  LANGUAGE SQL
AS $$
  SELECT pessoas_us.id_pessoa
  FROM 
    public.pessoas_geram_usuarios as pessoas_us,
    auth.usuarios as users
    WHERE  
        email = users.email_usuario AND
        users.id_usuario = pessoas_us.id_usuario
$$;

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION
public.get_disciplinas_plano(aluno bigint, plano bigint)
  RETURNS TABLE(id_disciplina bigint)
  LANGUAGE SQL
AS $$
  SELECT planejamento.id_disciplina 
  FROM alunos_planejam_disciplinas as planejamento
    WHERE 
        aluno = planejamento.id_pessoa AND
        plano = planejamento.id_plano

$$;

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION
public.all_professores()
  RETURNS TABLE(id_pessoa bigint, nusp_professor varchar(10), departamento varchar(100), -- professores
                nome varchar(280), -- pessoas
                email_usuario varchar(256), cpf character(11), sala varchar(20)) -- usuarios
                -- professores_oferecem_disciplinas
  LANGUAGE SQL  
AS $$
  SELECT professores.id_pessoa, professores.nusp_professor, professores.departamento,
         pessoas.nome,
         usuarios.email_usuario, pessoas.cpf, professores.sala
  FROM
    public.professores as professores,
    public.pessoas as pessoas,
    auth.usuarios as usuarios,
    public.pessoas_geram_usuarios as pgu
    WHERE
      professores.id_pessoa = pessoas.id_pessoa AND
      pgu.id_pessoa = professores.id_pessoa AND
      pgu.id_usuario = usuarios.id_usuario
$$;

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION
public.get_creditos_obrigatorias_feitos(aluno bigint)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT (SUM(disciplinas.creditos_aula) + SUM(disciplinas.creditos_trabalho))
  FROM 
    public.alunos_cursam_disciplinas as cursar,
    public.grade_obrigatoria as grade,
    public.professores_oferecem_disciplinas as oferecimento,
    public.disciplinas as disciplinas
    WHERE 
        cursar.id_pessoa = aluno AND
        grade.id_disciplina = oferecimento.id_disciplina AND  
        cursar.id_oferecimento = oferecimento.id_oferecimento AND
        disciplinas.id_disciplina = oferecimento.id_disciplina AND
        (cursar.nota IS NOT NULL)
$$;

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION
public.get_creditos_obrigatorias_fazendo(aluno bigint)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT (SUM(disciplinas.creditos_aula) + SUM(disciplinas.creditos_trabalho))
  FROM 
    public.alunos_cursam_disciplinas as cursar,
    public.grade_obrigatoria as grade,
    public.professores_oferecem_disciplinas as oferecimento,
    public.disciplinas as disciplinas
    WHERE 
        cursar.id_pessoa = aluno AND
        grade.id_disciplina = oferecimento.id_disciplina AND  
        cursar.id_oferecimento = oferecimento.id_oferecimento AND
        disciplinas.id_disciplina = oferecimento.id_disciplina AND
        (cursar.nota IS NULL)
$$;

--------------------------------------------------------------------------------  

CREATE OR REPLACE FUNCTION
public.get_creditos_eletivas_feitos(aluno bigint)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT (SUM(disciplinas.creditos_aula) + SUM(disciplinas.creditos_trabalho))
  FROM 
    public.alunos_cursam_disciplinas as cursar,
    public.grade_optativa as grade,
    public.professores_oferecem_disciplinas as oferecimento,
    public.disciplinas as disciplinas
    WHERE 
        cursar.id_pessoa = aluno AND
        grade.id_disciplina = oferecimento.id_disciplina AND  
        cursar.id_oferecimento = oferecimento.id_oferecimento AND
        disciplinas.id_disciplina = oferecimento.id_disciplina AND
        (cursar.nota IS NOT NULL) AND
        grade.eletiva = true
$$;

--------------------------------------------------------------------------------  

CREATE OR REPLACE FUNCTION
public.get_creditos_eletivas_fazendo(aluno bigint)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT (SUM(disciplinas.creditos_aula) + SUM(disciplinas.creditos_trabalho))
  FROM 
    public.alunos_cursam_disciplinas as cursar,
    public.grade_optativa as grade,
    public.professores_oferecem_disciplinas as oferecimento,
    public.disciplinas as disciplinas
    WHERE 
        cursar.id_pessoa = aluno AND
        grade.id_disciplina = oferecimento.id_disciplina AND  
        cursar.id_oferecimento = oferecimento.id_oferecimento AND
        disciplinas.id_disciplina = oferecimento.id_disciplina AND
        (cursar.nota IS NULL) AND
        grade.eletiva = true
$$;

--------------------------------------------------------------------------------  

CREATE OR REPLACE FUNCTION
public.get_creditos_livres_feitos(aluno bigint)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT (SUM(disciplinas.creditos_aula) + SUM(disciplinas.creditos_trabalho))
  FROM 
    public.alunos_cursam_disciplinas as cursar,
    public.grade_optativa as grade,
    public.professores_oferecem_disciplinas as oferecimento,
    public.disciplinas as disciplinas
    WHERE 
        cursar.id_pessoa = aluno AND
        grade.id_disciplina = oferecimento.id_disciplina AND  
        cursar.id_oferecimento = oferecimento.id_oferecimento AND
        disciplinas.id_disciplina = oferecimento.id_disciplina AND
        (cursar.nota IS NOT NULL) AND
        grade.eletiva = false
$$;

--------------------------------------------------------------------------------  

CREATE OR REPLACE FUNCTION
public.get_creditos_livres_fazendo(aluno bigint)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT (SUM(disciplinas.creditos_aula) + SUM(disciplinas.creditos_trabalho))
  FROM 
    public.alunos_cursam_disciplinas as cursar,
    public.grade_optativa as grade,
    public.professores_oferecem_disciplinas as oferecimento,
    public.disciplinas as disciplinas
    WHERE 
        cursar.id_pessoa = aluno AND
        grade.id_disciplina = oferecimento.id_disciplina AND  
        cursar.id_oferecimento = oferecimento.id_oferecimento AND
        disciplinas.id_disciplina = oferecimento.id_disciplina AND
        (cursar.nota IS NULL) AND
        grade.eletiva = false
$$;

--------------------------------------------------------------------------------  

CREATE OR REPLACE FUNCTION
public.cria_usuario(email varchar(256), senha varchar(256))
  RETURNS TABLE(id_usuario uuid)
  LANGUAGE PLPGSQL
AS $$
BEGIN
  INSERT INTO
  auth.usuarios(email_usuario, senha, expira)
  VALUES
  (email, senha, '2029-01-01');
  
  RETURN QUERY (SELECT us.id_usuario FROM auth.usuarios as us WHERE us.email_usuario = email);
END
$$;

------
GRANT ALL PRIVILEGES ON auth.usuarios TO web;
