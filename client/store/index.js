import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
})

export default () => {
  return new Vuex.Store({
    plugins: [vuexLocal.plugin],

    state: {
      email: '',
      token: '',
      id_pessoa: -1,
      info: '',
      disc_feitas: [], 
      disc_fazendo: [],
      is_admin: false,
    },

    getters: {
      isAuthenticated(state) {
        return state.token !== '';
      },
      getInfo(state) {
        return state.info; 
      },
      getIdPessoa(state) {
        return state.id_pessoa;
      },
      getDiscFeitas(state) {
        return state.disc_feitas;
      },
      getDiscFazendo(state) {
        return state.disc_fazendo;
      },
      isAdmin(state) {
        return state.is_admin;
      },
      getToken(state) {
        return state.token;
      }
    },

    mutations: {
      setEmail(state, { email }) {
        state.email = email;
      },

      setToken(state, { token }) {
        state.token = token;
      },
      setDiscInfo(state, { info }) {
        state.info = info;
      },
      setIdPessoa(state, { id_pessoa }) {
        state.id_pessoa = id_pessoa;
      },
      resetState(state) {
        state.token = '', 
        state.id_pessoa = -1,
        state.disc_feitas = [], 
        state.disc_fazendo = []
      },
      setDiscFeitas(state, {disc_feitas}) {
        state.disc_feitas = disc_feitas
      },
      setDiscFazendo(state, {disc_fazendo}) {
        state.disc_fazendo = disc_fazendo
      },
      setIsAdmin(state, {is_admin}) {
        state.is_admin = is_admin
      }
    }
  });
}
